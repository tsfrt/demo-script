

Select the minimal base image that meets your needs, for instance a statically compiled GO application does not require any OS level libraries or services while dotnet, java, node generally require OS level libraries to support their runtime layer.  Reducing the surface area of the image ensures that extraneous CVEs are not encountered.  Curating and maintaining these base image options is a significant effort for an engineering team.

```bash

tseufert-a01:docs tseufert$ kp clusterstack list
NAME       READY    ID
base       True     io.buildpacks.stacks.bionic
default    True     io.buildpacks.stacks.bionic
full       True     io.buildpacks.stacks.bionic
tiny       True     io.paketo.stacks.tiny

```

Clusterstores manage the life-cycle of a set of Cloud Native Buildpacks within your cluster.  These Cloud Native Build Packs bundle app artifacts and build tools and supply logic that determines which tools and components are required for an appliction image.  Each managed component is rigoursously scanned for CVEs, and know vulnerabilites.  

```

kp clusterstore list
NAME                READY
default             True
js-cluster-store    True

tseufert-a01:docs tseufert$ kp clusterstore status default
Status:    Ready

BUILDPACKAGE ID                       VERSION    HOMEPAGE
paketo-buildpacks/procfile            4.2.2      https://github.com/paketo-buildpacks/procfile
tanzu-buildpacks/dotnet-core          1.5.1      https://docs.pivotal.io/tanzu-buildpacks/dotnet-core/dotnet-core-buildpack.html
tanzu-buildpacks/go                   1.6.0      https://docs.pivotal.io/tanzu-buildpacks/go/go-buildpack.html
tanzu-buildpacks/go                   1.5.0      https://docs.pivotal.io/tanzu-buildpacks/go/go-buildpack.html
tanzu-buildpacks/httpd                0.2.1      https://docs.pivotal.io/tanzu-buildpacks/httpd/httpd-buildpack.html
tanzu-buildpacks/java                 5.12.0     https://github.com/pivotal-cf/tanzu-java
tanzu-buildpacks/java                 5.11.0     https://github.com/pivotal-cf/tanzu-java
tanzu-buildpacks/java-native-image    4.4.0      https://github.com/pivotal-cf/tanzu-java-native-image
tanzu-buildpacks/java-native-image    4.3.0      https://github.com/pivotal-cf/tanzu-java-native-image
tanzu-buildpacks/nginx                0.2.0      https://docs.pivotal.io/tanzu-buildpacks/nginx/nginx-buildpack.html
tanzu-buildpacks/nodejs               1.9.1      https://docs.pivotal.io/tanzu-buildpacks/nodejs/nodejs-buildpack.html
tanzu-buildpacks/nodejs               1.9.2      https://docs.pivotal.io/tanzu-buildpacks/nodejs/nodejs-buildpack.html
tanzu-buildpacks/nodejs               1.9.3      https://docs.pivotal.io/tanzu-buildpacks/nodejs/nodejs-buildpack.html
tanzu-buildpacks/php                  0.3.0      https://docs.pivotal.io/tanzu-buildpacks/php/php-buildpack.html
tanzu-buildpacks/python               1.0.1      

```

The stack and store configuration get combineded into a component called a builder.  The builder is used to build a specific user application using the configured store+stack combination.

```
tseufert-a01:docs tseufert$ kp clusterbuilder list
NAME                  READY    STACK                          IMAGE
base                  true     io.buildpacks.stacks.bionic    harbor.proto.tsfrt.net/tbs/build-service/base@sha256:46066514ceb258609fff66b67bc8a918bed6a3ac18eaedadccab8881ae7257fa
default               true     io.buildpacks.stacks.bionic    harbor.proto.tsfrt.net/tbs/build-service/default@sha256:46066514ceb258609fff66b67bc8a918bed6a3ac18eaedadccab8881ae7257fa
full                  true     io.buildpacks.stacks.bionic    harbor.proto.tsfrt.net/tbs/build-service/full@sha256:703a567ee8769e5fad4e9d313eab11632941064881a1feb5fb6184e2c2f47a60
js-cluster-builder    true     io.buildpacks.stacks.bionic    harbor.proto.tsfrt.net/builders/js-cluster-builder:0.0.1@sha256:3b74d5da21506b77f1139a1b003c11ed3247bc184bedda80205b3a357c4c5648
tiny                  true     io.paketo.stacks.tiny          harbor.proto.tsfrt.net/tbs/build-service/tiny@sha256:a6d5d1320903d04deb9763a451d0bb04a3f9352006b312907a6d5880836e284d

```

When an application is ready to be deployed to a container runtime, TBS is used to build an image using the appropriate builder, along with a developer's code.  A developer can use the `kp image create` command to create a container image in a supplied OCI container registry.

```
#note you need to create a secret in order to push images to a container registry
kp secret create harbor --registry harbor.proto.tsfrt.net --registry-user admin

#now that we can access the registry, we can create the image
kp image create test-image --tag harbor.proto.tsfrt.net/supply-chain/spring-music --git https://gitlab.com/delivery-mgmt/spring-music.git --cluster-builder base --wait

```

The build output will follow the process of the builder applying the correct buildpacks to build/publish the application, apply the OS Stack layers, and push the resulting image to the registry defined in our tag.

```
#build pack detection
===> DETECT
7 of 33 buildpacks participating
paketo-buildpacks/ca-certificates   2.3.2
paketo-buildpacks/bellsoft-liberica 8.2.0
paketo-buildpacks/gradle            5.4.0
paketo-buildpacks/executable-jar    5.1.2
paketo-buildpacks/apache-tomcat     6.0.0
paketo-buildpacks/dist-zip          4.1.2
paketo-buildpacks/spring-boot       4.4.2

#this app happens to use Gradle, so the gradle build tool is invoked
Downloading https://services.gradle.org/distributions/gradle-6.1-bin.zip
............................................................................................
Unzipping /home/cnb/.gradle/wrapper/dists/gradle-6.1-bin/74ow5f78ml5iin9568hvtcqz9/gradle-6.1-bin.zip to /home/cnb/.gradle/wrapper/dists/gradle-6.1-bin/74ow5f78ml5iin9568hvtcqz9
Set executable permissions for: /home/cnb/.gradle/wrapper/dists/gradle-6.1-bin/74ow5f78ml5iin9568hvtcqz9/gradle-6.1/bin/gradle

Welcome to Gradle 6.1!...

#A container is built reusing any layers that have not changed (from previous build not docker cache)
===> EXPORT
Adding layer 'paketo-buildpacks/ca-certificates:helper'
Adding layer 'paketo-buildpacks/bellsoft-liberica:helper'
Adding layer 'paketo-buildpacks/bellsoft-liberica:java-security-properties'
Adding layer 'paketo-buildpacks/bellsoft-liberica:jre'
Adding layer 'paketo-buildpacks/bellsoft-liberica:jvmkill'
Adding layer 'paketo-buildpacks/executable-jar:classpath'
Adding layer 'paketo-buildpacks/spring-boot:helper'
Adding layer 'paketo-buildpacks/spring-boot:spring-cloud-bindings'
Adding layer 'paketo-buildpacks/spring-boot:web-application-type'
Adding 1/1 app layer(s)
Adding layer 'launcher'
Adding layer 'config'
Adding layer 'process-types'
Adding label 'io.buildpacks.lifecycle.metadata'
Adding label 'io.buildpacks.build.metadata'
Adding label 'io.buildpacks.project.metadata'
Adding label 'org.springframework.boot.version'
Setting default process type 'web'
Saving harbor.proto.tsfrt.net/supply-chain/spring-music...
*** Images (sha256:e9fc092a25468a62f6d72d88c6ed061dbd257adac37471e8b109e4fac925365a):
      harbor.proto.tsfrt.net/supply-chain/spring-music
      harbor.proto.tsfrt.net/supply-chain/spring-music:b1.20210817.162121
Adding cache layer 'paketo-buildpacks/bellsoft-liberica:jdk'
Adding cache layer 'paketo-buildpacks/gradle:application'
Adding cache layer 'paketo-buildpacks/gradle:cache'

```

go to harbor https://harbor.proto.tsfrt.net/harbor/projects and check out the supply-chain project.

check out the bom produced by TBS

```

#using jq makes it easier to read
kp build status  test-image --bom | jq .

```

run your image

```
#login to your registry
docker login harbor.proto.tsfrt.net

docker run -d -p 8089:8080 harbor.proto.tsfrt.net/supply-chain/spring-music:latest

```
open the browser and checkout http://localhost:8089/ note the port you have mapped
